package com.sqldaolunes.entity;

public class Catalogo {

    private Long idCatalogo;
    private String nombre;

    public Catalogo() {
    }

    public Catalogo(String nombre) {
        this.nombre = nombre;
    }

    public Long getIdCatalogo() {
        return idCatalogo;
    }

    public void setIdCatalogo(Long idCatalogo) {
        this.idCatalogo = idCatalogo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
