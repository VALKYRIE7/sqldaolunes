package com.sqldaolunes.entity;


public class Especificacion {

    private Long idEspecificacion;
    private String nombre;
    private double precio;
    private Long fk_catalogos;

    public Especificacion() {
    }

    public Especificacion(String nombre, double precio) {
        this.nombre = nombre;
        this.precio = precio;
    }

    public Especificacion(String nombre, double precio, Long fk_catalogos) {
        this.nombre = nombre;
        this.precio = precio;
        this.fk_catalogos = fk_catalogos;
    }

    public Long getIdEspecificacion() {
        return idEspecificacion;
    }

    public void setIdEspecificacion(Long idEspecificacion) {
        this.idEspecificacion = idEspecificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Long getFk_catalogos() {
        return fk_catalogos;
    }

    public void setFk_catalogos(Long fk_catalogos) {
        this.fk_catalogos = fk_catalogos;
    }

}
