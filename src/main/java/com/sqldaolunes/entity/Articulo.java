package com.sqldaolunes.entity;

public class Articulo {

    private Long idArticulo;
    private String nombre;
    private Long fk_especificaciones;

    public Articulo() {
    }

    public Articulo(String nombre) {
        this.nombre = nombre;
    }

    public Articulo(String nombre, Long fk_especificaciones) {
        this.nombre = nombre;
        this.fk_especificaciones = fk_especificaciones;
    }

    public Long getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(Long idArticulo) {
        this.idArticulo = idArticulo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getFk_especificaciones() {
        return fk_especificaciones;
    }

    public void setFk_especificaciones(Long fk_especificaciones) {
        this.fk_especificaciones = fk_especificaciones;
    }

}
