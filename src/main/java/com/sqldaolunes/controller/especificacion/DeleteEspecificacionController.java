package com.sqldaolunes.controller.especificacion;

import com.sqldaolunes.dao.ISqlDao;
import com.sqldaolunes.dao.impl.EspecificacionDaoImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/especificacion/delete")
public class DeleteEspecificacionController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public DeleteEspecificacionController() {
        // Do Nothing
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String idEspecificacion = request.getParameter("idEspecificacion");

        if(idEspecificacion == "" || idEspecificacion == null)
            request.getRequestDispatcher("/").forward(request, response);
        else {
            Long id = Long.parseLong(idEspecificacion);
            ISqlDao iSqlDao = EspecificacionDaoImpl.getInstance();

            iSqlDao.delete(id);

            response.sendRedirect(request.getContextPath() + "/");
        }
    }
}
