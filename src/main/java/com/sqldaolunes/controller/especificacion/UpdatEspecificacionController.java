package com.sqldaolunes.controller.especificacion;

import com.sqldaolunes.dao.ISqlDao;
import com.sqldaolunes.dao.impl.EspecificacionDaoImpl;
import com.sqldaolunes.entity.Especificacion;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/especificacion/update")
public class UpdatEspecificacionController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private final ISqlDao iSqlDao = EspecificacionDaoImpl.getInstance();

    public UpdatEspecificacionController() {
        // Do Nothing
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Long idEspecificacion = Long.parseLong(request.getParameter("idEspecificacion"));

        //String nombre = request.getParameter("nombre");
       // Double precio = Double.parseDouble(request.getParameter("precio"));

        Especificacion espec = (Especificacion) iSqlDao.readById(idEspecificacion);
        request.setAttribute("espec", espec);

        //request.setAttribute("idEspecificacion",);
        //request.setAttribute("nombre",nombre);

        //Especificacion especificacion = new Especificacion(idEspecificacion, nombre);


        //iSqlDao.update(especificacion);
        request.getRequestDispatcher("/").forward(request, response);
       // response.sendRedirect(request.getContextPath() + "/");
    }


}


//      String idEspecificacion = request.getParameter("idEspecificacion");
//        String nombre = request.getParameter("nombre");
//       Double precio = Double.parseDouble(request.getParameter("precio"));
//Long fk_catalogos = Long.parseLong(request.getParameter("fk_catalogos"));
