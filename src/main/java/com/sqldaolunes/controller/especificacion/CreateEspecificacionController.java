package com.sqldaolunes.controller.especificacion;

import com.sqldaolunes.dao.ISqlDao;
import com.sqldaolunes.dao.impl.EspecificacionDaoImpl;
import com.sqldaolunes.entity.Especificacion;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/especificacion/create")
public class CreateEspecificacionController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private final ISqlDao iSqlDao = EspecificacionDaoImpl.getInstance();


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String idEspecificacion = request.getParameter("idEspecificacion");
        String nombre = request.getParameter("nombre");
        Double precio = Double.parseDouble(request.getParameter("precio"));
        Long fk_catalogos = Long.parseLong(request.getParameter("fk_catalogos"));

        Especificacion espec = new Especificacion(nombre, precio,fk_catalogos);

        if (idEspecificacion == null || idEspecificacion == "") {
            iSqlDao.create(espec);
        } else {
            Long id = Long.parseLong(idEspecificacion);
            espec.setIdEspecificacion(id);
            iSqlDao.update(espec);
        }
        response.sendRedirect(request.getContextPath() + "/");
    }

}

//    private Long idEspecificacion;
//    private String nombre;
//    private double precio;
//    private Long fk_catalogos;