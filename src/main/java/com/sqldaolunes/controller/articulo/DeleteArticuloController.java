package com.sqldaolunes.controller.articulo;

import com.sqldaolunes.dao.ISqlDao;
import com.sqldaolunes.dao.impl.ArticuloDaoImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/articulo/delete")
public class DeleteArticuloController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public DeleteArticuloController() {
        // Do Nothing
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String idArticulo = request.getParameter("idArticulo");

        if(idArticulo == "" || idArticulo == null)
            request.getRequestDispatcher("/").forward(request, response);
        else {
            Long id = Long.parseLong(idArticulo);
            ISqlDao iSqlDao = ArticuloDaoImpl.getInstance();

            iSqlDao.delete(id);

            response.sendRedirect(request.getContextPath() + "/");
        }
    }
}
