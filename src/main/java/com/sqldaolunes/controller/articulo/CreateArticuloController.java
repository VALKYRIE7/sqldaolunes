package com.sqldaolunes.controller.articulo;

import com.sqldaolunes.dao.ISqlDao;
import com.sqldaolunes.dao.impl.ArticuloDaoImpl;
import com.sqldaolunes.entity.Articulo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/articulo/create")
public class CreateArticuloController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private ISqlDao iSqlDao = ArticuloDaoImpl.getInstance();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String idArticulo = request.getParameter("idArticulo");
        String nombre = request.getParameter("nombre");
        Long fk_especificaciones = Long.parseLong(request.getParameter("fk_especificaciones"));

        Articulo artic = new Articulo(nombre, fk_especificaciones);

        if (idArticulo == null || idArticulo == "") {
            iSqlDao.create(artic);
        } else {
            Long id = Long.parseLong(idArticulo);
            artic.setIdArticulo(id);
            iSqlDao.update(artic);
        }
        response.sendRedirect(request.getContextPath() + "/");
    }

}

/*
    private Long idArticulo;
    private String nombre;
    private Long fk_especificaciones;

 */
