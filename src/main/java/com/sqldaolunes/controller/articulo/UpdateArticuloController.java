package com.sqldaolunes.controller.articulo;

import com.sqldaolunes.dao.ISqlDao;
import com.sqldaolunes.dao.impl.ArticuloDaoImpl;
import com.sqldaolunes.entity.Articulo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/articulo/update")
public class UpdateArticuloController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private final ISqlDao iSqlDao = ArticuloDaoImpl.getInstance();

    public UpdateArticuloController() {
        // Do Nothing
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Long idArticulo = Long.parseLong(request.getParameter("idArticulo"));

        Articulo artic = (Articulo) iSqlDao.readById(idArticulo);

        request.setAttribute("artic", artic);

        request.getRequestDispatcher("/").forward(request, response);
    }
}