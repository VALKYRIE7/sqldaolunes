package com.sqldaolunes.controller.catalogo;

import com.sqldaolunes.dao.ISqlDao;
import com.sqldaolunes.dao.impl.CatalogoDaoImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/catalogo/delete")
public class DeleteCatalogoController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public DeleteCatalogoController() {
        // Do Nothing
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String idCatalogo = request.getParameter("idCatalogo");

        if(idCatalogo == "" || idCatalogo == null)
            request.getRequestDispatcher("/").forward(request, response);
        else {
            Long id = Long.parseLong(idCatalogo);
            ISqlDao iSqlDao = CatalogoDaoImpl.getInstance();

            iSqlDao.delete(id);

            response.sendRedirect(request.getContextPath() + "/");
        }


    }

}
