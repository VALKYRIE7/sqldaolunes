package com.sqldaolunes.controller.catalogo;


import com.sqldaolunes.dao.ISqlDao;
import com.sqldaolunes.dao.impl.CatalogoDaoImpl;
import com.sqldaolunes.entity.Catalogo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/catalogo/create")
public class CreateCatalogoController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private ISqlDao iSqlDao = CatalogoDaoImpl.getInstance();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/").forward(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String idCatalogo = request.getParameter("idCatalogo");
        String nombre = request.getParameter("nombre");

        Catalogo catal = new Catalogo(nombre);

        if (idCatalogo == null || idCatalogo == "") {
            iSqlDao.create(catal);
        } else {
            Long id = Long.parseLong(idCatalogo);
            catal.setIdCatalogo(id);
            iSqlDao.update(catal);
        }
        response.sendRedirect(request.getContextPath() + "/");
    }

}

   // private Long idCatalogo;
   // private String nombre;