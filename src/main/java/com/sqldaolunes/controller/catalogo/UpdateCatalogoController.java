package com.sqldaolunes.controller.catalogo;

import com.sqldaolunes.dao.ISqlDao;
import com.sqldaolunes.dao.impl.CatalogoDaoImpl;
import com.sqldaolunes.entity.Catalogo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/catalogo/update")
public class UpdateCatalogoController extends HttpServlet {

    private final ISqlDao iSqlDao = CatalogoDaoImpl.getInstance();

    public UpdateCatalogoController() {
        // Do Nothing
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Long idCatalogo = Long.parseLong(request.getParameter("idCatalogo"));

        Catalogo catal = (Catalogo) iSqlDao.readById(idCatalogo);

        request.setAttribute("catal",catal);

        request.getRequestDispatcher("/").forward(request, response);

    }
}
