package com.sqldaolunes.controller;


import com.sqldaolunes.dao.ISqlDao;
import com.sqldaolunes.dao.impl.ArticuloDaoImpl;
import com.sqldaolunes.dao.impl.CatalogoDaoImpl;
import com.sqldaolunes.dao.impl.EspecificacionDaoImpl;
import com.sqldaolunes.entity.Articulo;
import com.sqldaolunes.entity.Catalogo;
import com.sqldaolunes.entity.Especificacion;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/")
public class HomeController extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private final ISqlDao iSqlDao = EspecificacionDaoImpl.getInstance();
    private final ISqlDao iSqlDaoCat = CatalogoDaoImpl.getInstance();
    private final ISqlDao iSqlDaoArt = ArticuloDaoImpl.getInstance();


    public HomeController() {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<Especificacion> especificaciones = iSqlDao.readAll();
        request.setAttribute("especificacionList", especificaciones);

        List<Catalogo> catalogos = iSqlDaoCat.readAll();
        request.setAttribute("catalogoList", catalogos);

        List<Articulo> articulos = iSqlDaoArt.readAll();
        request.setAttribute("articuloList", articulos);

        request.getRequestDispatcher("home.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
