package com.sqldaolunes.dao.impl;


import com.sqldaolunes.dao.ISqlDao;
import com.sqldaolunes.db.Database;
import com.sqldaolunes.entity.Catalogo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CatalogoDaoImpl implements ISqlDao<Catalogo> {

    private static CatalogoDaoImpl catalogoDaoImpl = null;

    private Connection connection = Database.getConnection();

    public static ISqlDao getInstance() {
        if (catalogoDaoImpl == null) {
            catalogoDaoImpl = new CatalogoDaoImpl();
        }
        return catalogoDaoImpl;
    }

    @Override
    public long create(Catalogo ob) {
        String sql = "INSERT INTO catalogos(nombre) VALUES(?)";
        long id = 0;
        try {
            PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, ob.getNombre());

            if (pstmt.executeUpdate() > 0) {
                ResultSet rs = pstmt.getGeneratedKeys();

                if (rs.next()) {
                    id = rs.getLong(1);
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return id;
    }

    @Override
    public List<Catalogo> readAll() {
        String sql = "SELECT * FROM catalogos";
        List<Catalogo> catalogos = null;
        try {
            PreparedStatement pstmt = connection.prepareStatement(sql);

            ResultSet resultSet = pstmt.executeQuery();
            while (resultSet.next()) {
                if (catalogos == null) {
                    catalogos = new ArrayList<>();
                }

                Catalogo catal = new Catalogo();
                catal.setIdCatalogo(resultSet.getLong(1));
                catal.setNombre(resultSet.getString(2));

                catalogos.add(catal);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return catalogos;

    }

    @Override
    public void update(Catalogo ob) {
        String sql = "UPDATE catalogos SET nombre=? WHERE idCatalogo=?";

        try{
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setString(1,ob.getNombre());
            pstmt.setLong(2,ob.getIdCatalogo());

            pstmt.executeUpdate();
            pstmt.close();

        }catch (Exception ex) {
            System.out.println(ex.getMessage());
        }


    }

    @Override
    public void delete(Long id) {

        String sql = "DELETE FROM catalogos WHERE idCatalogo=?";

        try {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setLong(1, id);

            // Delete Customer Account
            pstmt.executeUpdate();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public Catalogo readById(Long id) {
        String sql = "SELECT * FROM catalogos WHERE idCatalogo=?";

        try {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setLong(1, id);

            // Getting Customer's Detail
            ResultSet resultSet = pstmt.executeQuery();

            if (resultSet.next()) {
                Catalogo catal = new Catalogo();

                catal.setIdCatalogo(resultSet.getLong(1));
                catal.setNombre(resultSet.getString(2));

                return catal;

            }
            resultSet.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return null;
    }

}