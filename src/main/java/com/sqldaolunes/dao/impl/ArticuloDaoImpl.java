package com.sqldaolunes.dao.impl;


import com.sqldaolunes.dao.ISqlDao;
import com.sqldaolunes.db.Database;
import com.sqldaolunes.entity.Articulo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ArticuloDaoImpl implements ISqlDao<Articulo> {

    private static ArticuloDaoImpl articuloDaoImpl = null;

    private Connection connection = Database.getConnection();

    public static ISqlDao getInstance() {
        if (articuloDaoImpl == null) {
            articuloDaoImpl = new ArticuloDaoImpl();
        }
        return articuloDaoImpl;
    }

    @Override
    public long create(Articulo ob) {
        String sql = "INSERT INTO articulos(nombre, fk_especificaciones) VALUES(?,?)";
        long id = 0;
        try {
            PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, ob.getNombre());
            pstmt.setLong(2, ob.getFk_especificaciones());
            //pstmt.setDouble(2, ob.getPrecio());

            if (pstmt.executeUpdate() > 0) {
                ResultSet rs = pstmt.getGeneratedKeys();

                if (rs.next()) {
                    id = rs.getLong(1);
                }
            }
            pstmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return id;
    }

    @Override
    public List<Articulo> readAll() {
        String sql = "SELECT * FROM articulos";
        List<Articulo> articulos = null;
        try {
            PreparedStatement pstmt = connection.prepareStatement(sql);

            ResultSet resultSet = pstmt.executeQuery();
            while (resultSet.next()) {
                if (articulos == null) {
                    articulos = new ArrayList<>();
                }

                Articulo artic = new Articulo();
                artic.setIdArticulo(resultSet.getLong(1));
                artic.setNombre(resultSet.getString(2));
                artic.setFk_especificaciones(resultSet.getLong(3));


                articulos.add(artic);
            }
            pstmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return articulos;
    }

    @Override
    public void update(Articulo ob) {
        String sql = "UPDATE articulos SET nombre=?, fk_especificaciones=? WHERE idArticulo=?";

        try {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, ob.getNombre());
            pstmt.setLong(2, ob.getFk_especificaciones());
            pstmt.setLong(3, ob.getIdArticulo());

            pstmt.executeUpdate();
            pstmt.close();

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void delete(Long id) {
        String sql = "DELETE FROM articulos WHERE idArticulo=?";

        try {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setLong(1, id);

            // Delete Customer Account
            pstmt.executeUpdate();
            pstmt.close();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public Articulo readById(Long id) {
        String sql = "SELECT * FROM articulos WHERE idArticulo=?";

        try {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setLong(1, id);

            // Getting Customer's Detail
            ResultSet resultSet = pstmt.executeQuery();

            if (resultSet.next()) {
                Articulo artic = new Articulo();
                artic.setIdArticulo(resultSet.getLong(1));
                artic.setNombre(resultSet.getString(2));
                //artic.setFk_especificaciones(resultSet.getLong(3));

                return artic;
            }
            resultSet.close();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return null;
    }

}
