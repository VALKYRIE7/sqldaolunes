package com.sqldaolunes.dao.impl;

import com.sqldaolunes.dao.ISqlDao;
import com.sqldaolunes.db.Database;
import com.sqldaolunes.entity.Especificacion;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EspecificacionDaoImpl implements ISqlDao<Especificacion> {

    private static EspecificacionDaoImpl especificacionDaoImpl = null;

    private final Connection connection = Database.getConnection();

    public static ISqlDao getInstance() {
        if (especificacionDaoImpl == null) {
            especificacionDaoImpl = new EspecificacionDaoImpl();
        }

        return especificacionDaoImpl;
    }

    @Override
    public long create(Especificacion ob) {
        String sql = "INSERT INTO especificaciones(descripcion, precio, fk_catalogos) VALUES(?,?,?)";
        long id = 0;
        try {
            PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            pstmt.setString(1, ob.getNombre());
            pstmt.setDouble(2, ob.getPrecio());
            pstmt.setLong(3, ob.getFk_catalogos());

            if (pstmt.executeUpdate() > 0) {
                ResultSet rs = pstmt.getGeneratedKeys();

                if (rs.next()) {
                    id = rs.getLong(1);
                }
            }
            pstmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return id;
    }

    @Override
    public List<Especificacion> readAll() {
        String sql = "SELECT * FROM especificaciones";
        List<Especificacion> especificaciones = null;
        try {
            PreparedStatement pstmt = connection.prepareStatement(sql);

            // Getting Customer's Detail
            ResultSet resultSet = pstmt.executeQuery();
            while (resultSet.next()) {
                if (especificaciones == null) {
                    especificaciones = new ArrayList<>();
                }

                Especificacion espec = new Especificacion();
                espec.setIdEspecificacion(resultSet.getLong(1));
                espec.setNombre(resultSet.getString(2));
                espec.setPrecio(resultSet.getDouble(3));
                espec.setFk_catalogos(resultSet.getLong(4));


                especificaciones.add(espec);
            }

            //    resultSet.close();
            //   pstmt.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return especificaciones;
    }

    @Override
    public void update(Especificacion ob) {
        String sql = "UPDATE especificaciones SET descripcion=?, precio=?, fk_catalogos=? WHERE idEspecificacion=?";

        try {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, ob.getNombre());
            pstmt.setDouble(2, ob.getPrecio());
            pstmt.setLong(3, ob.getFk_catalogos());
            pstmt.setLong(4, ob.getIdEspecificacion());

            pstmt.executeUpdate();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void delete(Long id) {
        String sql = "DELETE FROM especificaciones WHERE idEspecificacion=?";

        try {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setLong(1, id);

            // Delete Customer Account
            pstmt.executeUpdate();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    @Override
    public Especificacion readById(Long id) {
        String sql = "SELECT * FROM especificaciones WHERE idEspecificacion=?";

        try {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setLong(1, id);

            // Getting Customer's Detail
            ResultSet resultSet = pstmt.executeQuery();

            if (resultSet.next()) {
                Especificacion espec = new Especificacion();
                espec.setIdEspecificacion(resultSet.getLong(1));
                espec.setNombre(resultSet.getString(2));
                espec.setPrecio(resultSet.getDouble(3));
                espec.setFk_catalogos(resultSet.getLong(4));
                // Long fk

                return espec;
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return null;
    }


    public void catalogoNombre() {

    }


}
