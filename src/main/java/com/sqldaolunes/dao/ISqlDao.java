package com.sqldaolunes.dao;

import java.util.List;

public interface ISqlDao<T> {

    long create(T ob);

    List<T> readAll();

    void update(T ob);

    void delete(Long id);

    T readById(Long id);
}