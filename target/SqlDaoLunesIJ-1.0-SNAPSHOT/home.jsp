<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP Page</title>
    <style type="text/css">
        body {
            text-align: center;
        }

        table {
            margin-left: 15%;
            min-width: 70%;
            border: 1px solid #CCC;
            border-collapse: collapse;
        }

        table tr {
            line-height: 30px;
        }

        table tr th {
            background: #000033;
            color: #FFF;
        }

        table tr td {
            border: 1px solid #CCC;
            margin: 5px;
        }

        input[type=text], input[type=email], input[type=tel] {
            min-width: 60%;
        }

        input[type=submit], a {
            background: green;
            padding: 5px;
            margin: 5px;
            color: #FFF;
        }

        a {
            text-decoration: none;
        }
    </style>

</head>
<body>
<h1>SQL DAO LUNES</h1>
<hr>
<br>

<!--
***********************
      CATÁLOGOS
***********************
-->
<h1>Tabla: CATÁLOGOS</h1>
<h2>Lista de catalogos</h2>

<c:url value="/catalogo/create" var="createCatal"/>
<form action="${createCatal}" method="post">
    <table>
        <c:if test="${catal.idCatalogo ne null}">
            <tr>
                <td>Catalogo ID:</td>
                <td><input type="text" name="idCatalogo" value="${catal.idCatalogo}"
                           readonly="readonly"></td>
            </tr>
        </c:if>
        <tr>
            <td>Nombre:</td>
            <td><input type="text" name="nombre"
                       value="${catal.nombre}" required></td>
        </tr>


        <c:if test="${catal.idCatalogo ne null}">
            <tr>
                <td colspan="2"><input type="submit" value="Update"></td>
            </tr>
        </c:if>
        <c:if test="${catal.idCatalogo eq null}">
            <tr>
                <td colspan="2"><input type="submit" value="Save"></td>
            </tr>
        </c:if>
    </table>
</form>
<br>


<table>
    <tr>
        <th>ID</th>
        <th>Nombre</th>

        <th>Update</th>
        <th>Delete</th>
    </tr>
    <c:forEach items="${catalogoList}" var="catalogos">
        <tr>
            <td>${catalogos.idCatalogo}</td>
            <td>${catalogos.nombre}</td>


            <td>
                <form action="<c:url value="/catalogo/update"/>" method="post">
                    <input type="hidden" name="idCatalogo" value="${catalogos.idCatalogo}">
                    <input type="submit" value="Update">
                </form>
            <td>
                <form action="<c:url value="/catalogo/delete"/>" method="post">
                    <input type="hidden" name="idCatalogo" value="${catalogos.idCatalogo}">
                    <input style="background: #F00;" type="submit" value="Delete">
                </form>
            </td>
        </tr>
    </c:forEach>
</table>

<br>
<hr>

<!--
***********************
      ESPECIFICACIONES
***********************
-->
<br>
<h1>Tabla: ESPECIFICACIONES</h1>
<h2>Lista de especificaciones</h2>
<br>


<c:url value="/especificacion/create" var="createEspec"/>
<form action="${createEspec}" method="post">
    <table>
        <c:if test="${espec.idEspecificacion ne null}">
            <tr>
                <td>Customer ID:</td>
                <td><input type="text" name="idEspecificacion" value="${espec.idEspecificacion}"
                           readonly="readonly"></td>
            </tr>
        </c:if>
        <tr>
            <td>Descripción:</td>
            <td><input type="text" name="nombre"
                       value="${espec.nombre}" required></td>
        </tr>
        <tr>
            <td>Precio:</td>
            <td><input type="number" name="precio"
                       value="${espec.precio}" required></td>
        </tr>

        <tr>
            <td>fk_catalogo</td>
            <td>
                <select name="fk_catalogos">
                    <c:forEach items="${catalogoList}" var="catalogos">

                        <option value="${catalogos.idCatalogo}">${catalogos.nombre}</option>
                    </c:forEach>

                </select>
            </td>
        </tr>

        <c:if test="${espec.idEspecificacion ne null}">
            <tr>
                <td colspan="2"><input type="submit" value="Update"></td>
            </tr>
        </c:if>
        <c:if test="${espec.idEspecificacion eq null}">
            <tr>
                <td colspan="2"><input type="submit" value="Save"></td>
            </tr>
        </c:if>


    </table>
</form>
<br>

<table>
    <tr>
        <th>ID</th>
        <th>Descripción</th>
        <th>Precio</th>
        <th>fk:Catalogo</th>
        <th>Update</th>
        <th>Delete</th>
    </tr>
    <c:forEach items="${especificacionList}" var="especificacion">
        <tr>
            <td>${especificacion.idEspecificacion}</td>
            <td>${especificacion.nombre}</td>
            <td>${especificacion.precio}</td>
            <td>${especificacion.fk_catalogos}</td>

            <td>
                <form action="<c:url value="/especificacion/update"/>" method="post">
                    <input type="hidden" name="idEspecificacion" value="${especificacion.idEspecificacion}">
                    <input type="submit" value="Update">
                </form>
            <td>
                <form action="<c:url value="/especificacion/delete"/>" method="post">
                    <input type="hidden" name="idEspecificacion" value="${especificacion.idEspecificacion}">
                    <input style="background: #F00;" type="submit" value="Delete">
                </form>
            </td>
        </tr>
    </c:forEach>
</table>

<br>
<hr>
<br>


<!--
***********************
      ARTICULOS
***********************
-->

<h1>Tabla: ARTÍCULOS</h1>
<h2>Lista de artículos</h2>
<br>

<c:url value="/articulo/create" var="createArtic"/>
<form action="${createArtic}" method="post">
    <table>
        <c:if test="${artic.idArticulo ne null}">
            <tr>
                <td>Articulo ID:</td>
                <td><input type="text" name="idArticulo" value="${artic.idArticulo}"
                           readonly="readonly"></td>
            </tr>
        </c:if>
        <tr>
            <td>Nombre:</td>
            <td><input type="text" name="nombre"
                       value="${artic.nombre}" required></td>
        </tr>


        <tr>
            <td>fk_especificaciones</td>
            <td>
                <select name="fk_especificaciones">
                    <c:forEach items="${especificacionList}" var="especificacion">

                        <option value="${especificacion.idEspecificacion}">${especificacion.nombre}</option>
                    </c:forEach>

                </select>
            </td>
        </tr>


        <c:if test="${artic.idArticulo ne null}">
            <tr>
                <td colspan="2"><input type="submit" value="Update"></td>
            </tr>
        </c:if>
        <c:if test="${artic.idArticulo eq null}">
            <tr>
                <td colspan="2"><input type="submit" value="Save"></td>
            </tr>
        </c:if>
    </table>
</form>
<br>


<table>
    <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>fk_especificaciones</th>
        <th>Update</th>
        <th>Delete</th>
    </tr>
    <c:forEach items="${articuloList}" var="articulos">
        <tr>
            <td>${articulos.idArticulo}</td>
            <td>${articulos.nombre}</td>
            <td>${articulos.fk_especificaciones}</td>

            <td>
                <form action="<c:url value="/articulo/update"/>" method="post">
                    <input type="hidden" name="idArticulo" value="${articulos.idArticulo}">
                    <input type="submit" value="Update">
                </form>
            <td>
                <form action="<c:url value="/articulo/delete"/>" method="post">
                    <input type="hidden" name="idArticulo" value="${articulos.idArticulo}">
                    <input style="background: #F00;" type="submit" value="Delete">
                </form>
            </td>
        </tr>
    </c:forEach>
</table>


</body>
</html>
